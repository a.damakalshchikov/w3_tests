import pytest
from selenium.webdriver.remote.webdriver import WebDriver
from selenium import webdriver
from ui.form import QueryForm


@pytest.fixture(scope="function")
def browser() -> WebDriver:
    driver = webdriver.Chrome()
    driver.get("https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all")
    yield driver
    driver.quit()


@pytest.fixture(scope="function")
def query_form(browser) -> QueryForm:
    form = QueryForm(driver=browser)
    yield form
