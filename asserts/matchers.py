from typing import Dict, TypeVar, Optional

from cattr import unstructure
from hamcrest import has_entries
from hamcrest.core.base_matcher import BaseMatcher
from hamcrest.core.description import Description

from ui.result_section import ResultSection

T = TypeVar("T")


class HasEntryWith(BaseMatcher[T]):
    def __init__(self, **kwargs):
        self.entry_fields_kw: Dict = kwargs

    def _matches(self, item: ResultSection) -> bool:
        if not hasattr(item, "entries"):
            return False

        entries = item.entries

        filtered_entries = list(filter(lambda x: has_entries(
            self.entry_fields_kw
        ).matches(unstructure(x)), entries))

        if len(filtered_entries) != 1:
            return False

        return True

    def describe_to(self, description: Description) -> None:
        description.append_description_of(f"Result entries don't contain any entry with {self.entry_fields_kw}")


def has_entry_with(**kwargs) -> HasEntryWith:
    return HasEntryWith(**kwargs)


class HasNumOfRecords(BaseMatcher[T]):
    def __init__(self, num_of_records: int):
        self.records = num_of_records
        self._actual_num_of_records: Optional[int] = None

    def _matches(self, item: ResultSection) -> bool:
        if not hasattr(item, "entries"):
            return False

        self._actual_num_of_records = len(item.entries)
        if self._actual_num_of_records != self.records:
            return False

        return True

    def describe_to(self, description: Description) -> None:
        description.append_description_of(f"Result number of entries expected "
                                          f"equal to {self.records}")

    def describe_mismatch(self, item: T, mismatch_description: Description) -> None:
        mismatch_description.append_text(f"was {self._actual_num_of_records}")


def has_num_of_records(num_of_records: int) -> BaseMatcher[T]:
    return HasNumOfRecords(num_of_records=num_of_records)
