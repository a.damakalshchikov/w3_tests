import pytest

from ui.form import QueryForm
from hamcrest.core.base_matcher import BaseMatcher
from asserts.matchers import has_entry_with, has_num_of_records


@pytest.mark.parametrize("query, matcher", [
    (
        "SELECT * FROM Customers;", has_entry_with(
            contact_name="Giovanni Rovelli",
            address="Via Ludovico il Moro 22")
    ),
    ("SELECT * FROM Customers WHERE city='London';", has_num_of_records(6))
])
def test_select_and_check_result(
    query_form: QueryForm,
    query: str,
    matcher: BaseMatcher
):
    query_form.query(query)
    query_form.assert_result(matcher)


@pytest.fixture(scope="function")
def query_form_with_inserted_value(browser):
    form = QueryForm(driver=browser)
    form.query_no_result("INSERT INTO Customers (CustomerName, ContactName, Address, City,PostalCode, Country)\
     VALUES ('Alexandr Damakalshchikov', 'Tim Moody', 'Groove Street 45', 'Los Santos', '462854', 'Romania');")
    yield form
    form.driver.find_element_by_id("restoreDBBtn").click()


def test_customer_insert(
    query_form_with_inserted_value: QueryForm
):
    query_form_with_inserted_value.query("SELECT * FROM Customers;")
    query_form_with_inserted_value.assert_result(has_entry_with(
        customer_name='Alexandr Damakalshchikov',
        contact_name='Tim Moody',
        address='Groove Street 45',
        city='Los Santos',
        postal_code='462854',
        country='Romania'
    )
    )


@pytest.fixture(scope="function")
def query_form_with_update_value(browser):
    form = QueryForm(driver=browser)
    form.query_no_result("UPDATE Customers \
     SET  CustomerName = 'Alexandr Damakalshchikov', ContactName = 'Tim Moody', Address = 'Groove Street 45',\
      City = 'Los Santos', PostalCode = '462854', Country = 'Romania'\
     WHERE CustomerName = 'Blauer See Delikatessen';")
    yield form
    form.driver.find_element_by_id("restoreDBBtn").click()


def test_update_entry(query_form_with_update_value: QueryForm):
    query_form_with_update_value.query("SELECT * FROM Customers;")
    query_form_with_update_value.assert_result(has_entry_with(
        customer_name='Alexandr Damakalshchikov',
        contact_name='Tim Moody',
        address='Groove Street 45',
        city='Los Santos',
        postal_code='462854',
        country='Romania'
    ))


@pytest.mark.xfail
def test_wrong_query(query_form: QueryForm):
    query_form.query("select from Customers;")
    query_form.assert_result(has_num_of_records(91))
