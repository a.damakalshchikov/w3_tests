from abc import ABC, abstractmethod

import allure
import attr
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


@attr.s(kw_only=True)
class Action(ABC):
    element = attr.ib()
    timeout: int = attr.ib(default=10)

    def perform_action(self):
        with allure.step(title="{0} to {1}".format(self.__class__.__name__, self.element)):
            self._perform()

    @abstractmethod
    def _perform(self, *args, **kwargs):
        raise NotImplementedError


@attr.s(kw_only=True)
class Click(Action):
    CLICK_RETRY: int = attr.ib(default=3)

    def _perform(self, *args, **kwargs):
        for i in range(self.CLICK_RETRY):
            try:
                element = WebDriverWait(self.element.driver, self.timeout).until(
                    EC.element_to_be_clickable(self.element.locator)
                )
                element.click()
                return
            except StaleElementReferenceException:
                if i == self.CLICK_RETRY - 1:
                    raise


@attr.s(kw_only=True)
class FillInput(Action):
    value: str = attr.ib()

    def _perform(self, *args, **kwargs):
        element: WebElement = WebDriverWait(self.element.driver, self.timeout).until(
            EC.presence_of_element_located(self.element.locator)
        )
        Click(element=self.element, timeout=self.timeout).perform_action()
        element.clear()
        element.send_keys(self.value)


@attr.s(kw_only=True)
class FillCodeMirror(Action):
    value: str = attr.ib()

    def _perform(self, *args, **kwargs):
        driver = self.element.driver
        driver.execute_script("arguments[0].CodeMirror.setValue(\"" + self.value + "\");", self.element.web_element)
