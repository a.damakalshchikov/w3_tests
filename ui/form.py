from typing import Optional, TypeVar

import attr
from hamcrest.core.base_matcher import BaseMatcher
from selenium.webdriver.remote.webdriver import WebDriver

from ui.request_section import RequestSection
from ui.result_section import ResultSection

T = TypeVar("T")


@attr.s(kw_only=True, slots=True)
class QueryForm:
    driver: WebDriver = attr.ib()
    request_section: RequestSection = attr.ib(default=None)
    result_section: Optional[ResultSection] = attr.ib(default=None)

    def __attrs_post_init__(self):
        self.request_section = RequestSection(driver=self.driver)

    def query_no_result(self, query: str):
        self.request_section.make_query(query_text=query)
        ResultSection(self.driver, no_entries=True).result_upd_completed()

    def query(self, query: str) -> None:
        self.request_section.make_query(query_text=query)
        self.result_section = ResultSection(driver=self.driver)

    def assert_result(self, matcher: BaseMatcher[T]):
        self.result_section.assert_result(matcher)
