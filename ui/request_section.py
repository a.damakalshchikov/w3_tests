from selenium.webdriver.common.by import By

from ui.basic_elements import CodeMirror, Button
import allure


class RequestSection:
    def __init__(self, driver):
        self.driver = driver
        self.query_field: CodeMirror = CodeMirror(locator=(By.CSS_SELECTOR, "#tryitform .CodeMirror"), driver=driver)
        self.run_query_btn: Button = Button(
            locator=(By.XPATH, "//button[contains(@onclick, 'runCodeSQL')]"),
            driver=driver
        )

    def make_query(self, query_text: str):
        with allure.step(title="Make query"):
            self.query_field.fill(query_text)
            self.run_query_btn.click()
