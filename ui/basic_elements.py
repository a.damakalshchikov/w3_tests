from typing import Tuple, List

import attr
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from ui.actions import Click, FillInput, FillCodeMirror
from ui.interfaces import Clickable, Fillable

LocatorType = Tuple[By, str]


class WrongEntriesCollection(Exception):
    pass


@attr.s(kw_only=True, slots=True)
class BaseElement:
    locator: LocatorType = attr.ib()
    driver: WebDriver = attr.ib()

    @property
    def web_element(self, timeout=10) -> WebElement:
        element: WebElement = WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located(self.locator)
        )
        return element


@attr.s(kw_only=True, slots=True)
class Button(BaseElement, Clickable):
    def click(self, timeout=5):
        Click(element=self, timeout=timeout).perform_action()


@attr.s(kw_only=True, slots=True)
class Input(BaseElement, Clickable, Fillable):
    def click(self, timeout=5):
        Click(element=self, timeout=timeout).perform_action()

    def fill(self, value: str):
        FillInput(element=self, value=value).perform_action()


@attr.s(kw_only=True, slots=True)
class CodeMirror(BaseElement, Clickable):
    def click(self, timeout=5):
        Click(element=self, timeout=timeout).perform_action()

    def fill(self, value: str):
        FillCodeMirror(element=self, value=value).perform_action()


@attr.s(kw_only=True, slots=True)
class Customer:
    customer_id: int = attr.ib()
    customer_name: str = attr.ib()
    contact_name: str = attr.ib()
    address: str = attr.ib()
    city: str = attr.ib()
    postal_code: str = attr.ib()
    country: str = attr.ib()


@attr.s(kw_only=True, slots=True)
class Frame(BaseElement):
    # метод ужасен, я не успевал TODO
    def get_customers_table_entries(self) -> List[Customer]:
        customers: List = []

        customer_locator = By.XPATH, "//div[@id='resultSQL']//tr[descendant::td]"
        num_of_records_locator = By.XPATH, "//div[contains(text(),'Number of Records')]"

        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(num_of_records_locator))

        num_of_records = int(
            self.driver.find_element(*num_of_records_locator).get_attribute("innerHTML").split(":")[1]
        )

        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(customer_locator))
        rows: List[WebElement] = self.driver.find_elements(*customer_locator)

        if num_of_records != len(rows):
            raise WrongEntriesCollection(f"Collected {len(rows)} out of {num_of_records} entries")

        for row in rows:
            tds = row.find_elements_by_tag_name("td")
            customers.append(
                Customer(
                    customer_id=tds[0].text,
                    customer_name=tds[1].text,
                    contact_name=tds[2].text,
                    address=tds[3].text,
                    city=tds[4].text,
                    postal_code=tds[5].text,
                    country=tds[6].text
                )
            )
        return customers

    def is_change_performed(self) -> bool:
        return bool(WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//div[contains(text(), 'You have made changes to the database')]")
            )
        ))
