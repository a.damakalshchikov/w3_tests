from abc import ABC, abstractmethod


class Clickable(ABC):
    @abstractmethod
    def click(self, timeout):
        raise NotImplementedError


class Fillable(ABC):
    @abstractmethod
    def fill(self, value):
        raise NotImplementedError
