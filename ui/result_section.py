from typing import List, TypeVar

import allure
from hamcrest import assert_that
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver

from ui.basic_elements import Customer, Frame

T = TypeVar("T")


class ResultSection:
    def __init__(self, driver: WebDriver, no_entries=False):
        self.driver = driver
        self.result_frame = Frame(driver=driver, locator=(By.ID, "iframeResultSQL"))
        self.entries: List[Customer] = self.result_frame.get_customers_table_entries() if not no_entries else []

    def result_upd_completed(self):
        return self.result_frame.is_change_performed()

    def assert_result(self, matcher):
        with allure.step(f"Assert"):
            assert_that(self, matcher)
